# Shapeshifter

Shapeshifter is a highly flexible framework for creating realtime 3D graphics
applications.

## Overview

Shapeshifter is code-generation based using a system of "interfaces" and targets
that provide the generation mechanisms for all interfaces that they support.

Users can write their own targets and interfaces, and can extend existing
targets to support more interfaces.

### Interfaces

* Core - Basic functionality and types
* Primitive - Common vertex buffers
* Transform - Tools for transforming vertex data.
* Animation - Complex animation and timeline systems
* Music - Music through an audio file
* WaveSabre - Music through the
    [WaveSabre](https://github.com/logicomacorp/WaveSabre) realtime synthesizer.

### Targets

* Cross-platform (`cross`)
* WebGL (`web`)
* [Windows only] 64-kilobyte (`64k`)

## Installation

TODO...

## Component Management

TODO...

## Editor

TODO...

## Generation Architecture

> **⚠️ ISSUES**
>
> This currently assumes a very GL-like API, which is not what the final product
should look like. Targets should be able to use whatever graphics API they want
(GL, Vulkan, etc.) without having any opinion forced on them.
>
> How dependencies between interfaces/plugins (e.g. a plugin may want to
invoke generation commands from `core`) will be handled is not clear.

At the most basic level, a target is a generation template and a build script
that compiles the generated code into a final executable. As an example, a basic
template may look like this:

```cpp
%INCLUDES%

%FUNCTIONS%

void main() {
    // set up gl context...

    %INIT%

    %FINALIZE%
    for (;;) {
        %RUN%
    }

    %CLEANUP%
}
```

...and the corresponding build script would compile and link this into a final
executable.

Interfaces are implemented for each target as executables that communicate with
the core Shapeshifter generator. To explain this process, consider the following
example project file:

> **⭐ Note**
>
> The `core::if` syntax is not in any way final, a completely different system
may be used instead. A subroutine system looks promising, but subroutines must
be inlined in order for some commands to work (e.g. quit). Additionally, making
a subroutine even for a single-command body is not very ergonomic.

```cpp
target("cross")
use("core", "primitive", "music")

load {
    primitive::quad("quad_verts")

    // buffers are implicitly created
    core::bind_buffer("verts", core::GL_ARRAY_BUFFER)
    core::buffer_data(core::GL_ARRAY_BUFFER, "quad_verts", core::GL_STATIC_DRAW)

    music::load("song.ogg")
}

finalize {
    music::start()
}

run {
    core::clear_color(0, 0, 0, 1)

    core::bind_buffer("verts", core::GL_ARRAY_BUFFER)

    core::enable_vertex_attrib(0, 0, 3, core::GL_FLOAT, false, 0, 0)
    // shaders are implicitly loaded
    core::draw(core::GL_TRIANGLE_STRIP, 0, 4, "test.vs", "test.fs")
    core::disable_vertex_attrib(0)

    core::if(music::done, {
        core::quit()
    })
}
```

...which will generate (approximately) the following code using our example
template:

```cpp
#include ...

GLuint compile_shader(...) {
    ...
}

GLuint shader_program(...) {
    ...
}

void main() {
    // set up gl context...

    // probably mangled var names in real generated code
    GLfloat quad_verts { ... };

    GLuint verts;
    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, verts);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quad_verts) vert_data, GL_STATIC_DRAW);

    GLuint test_vs = compile_shader("test.vs contents");
    GLuint test_fs = compile_shader("test.fs contents");
    GLuint test_test_prog = shader_program(test_vs, test_fs);

    // load and start music...

    for (;;) {
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        glBindBuffer(GL_ARRAY_BUFFER, verts);

        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

        glUseProgram(test_test_prog);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

        glDisableVertexAttribArray(0);

        if (music done) {
            break;
        }
    }

    // clean up everything (glDeleteX etc)
}
```

First, the Shapeshifter generator will parse the first lines of the project file
to figure out what target it is using. Then, it will load that target's template
files and query the available interface plugins for that target. The plugins
required by the project will then be started up. Plugins stay running during the
entire generation time and communicate with the core generator via `stdin` and
`stdout`.

> **⭐ Note**
>
> This textual representation of plugin communication is just for example. In
reality, plugins communicate with the generator using Cap 'n Proto. The schema
can be found in `protocol/`.

Immediately after being started, the generator queries each plugin for the types
that they define. As of now, the format of these type definitions has not been
figured out. After this, the generator loads the rest of the project and
and associates indicies with each each generation command. For example, consider
the previous example project again:

```cpp
load {
    [1] primitive::quad("quad_verts")

    [2] core::bind_buffer("verts", core::GL_ARRAY_BUFFER)
    [3] core::buffer_data(core::GL_ARRAY_BUFFER, "quad_verts", core::GL_STATIC_DRAW)

    [4] music::load("song.ogg")
}

finalize {
    [5] music::start()
}

run {
    [6] core::clear_color(0, 0, 0, 1)

    [7] core::bind_buffer("verts", core::GL_ARRAY_BUFFER)

    [8] core::enable_vertex_attrib(0, 0, 3, core::GL_FLOAT, false, 0, 0)
    [9] core::draw(core::GL_TRIANGLE_STRIP, 0, 4, "test.vs", "test.fs")
    [10] core::disable_vertex_attrib(0)

    [11] core::if(music::done, {
        [12] core::quit()
    })
}
```

After being indexed, each command is sent to it's corresponding plugin along
with it's index and what section it is in. After a plugin is fed all of it's
commands, it will determine what data it needs and what data it will provide
assuming further generation is successful. These requirements are then sent
to the core generator, and it will verify that all requirements will be
satisfied. Then, the generator will decide on mangled names for all of the
data that each plugin wants to delare and send them to each plugin. At this
point, each plugin can generate the code sections for each command using the
given data names. Each code section is sent back to the generator associated
with it's template section and command index. For instance, the `RUN` section
for the previous example program looks like this:

> **⭐ Note**
>
> This is assuming the use of subregions (older idea) for conditionals which is
not a final decision.

```json
"RUN": {
    "6": "
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT),
    ",
    "7": "
        glBindBuffer(GL_ARRAY_BUFFER, verts);
    ",
    "8": "
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    ",
    "9": "
        glUseProgram(test_test_prog);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    ",
    "10": "
        glDisableVertexAttribArray(0);
    ",
    "11": "
        if (music done) {
            %SUBREGION_0%
        }
    "
}
```

Finally, the generator can collect all of the generated sections from each
plugin and insert them into the corresponding sections of the target template,
ordered by the command index. Completed files are then written to disk and
compiled using the target build script.
