/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::error::Error;
use std::fmt;
use std::io::{self, Write};

use capnp::message::ReaderOptions;
use capnp::serialize as capnp_ser;
use shapeshifter_protocol as protocol;

pub trait PluginHandler {
    type Error: Error;

    fn typedefs(&mut self) -> Result<(), Self::Error>;
}

#[derive(Debug)]
pub enum PluginError<T: Error> {
    IoError(io::Error),
    CapnpError(capnp::Error),
    HandlerError(T),
}

impl<T: Error> From<io::Error> for PluginError<T> {
    fn from(v: io::Error) -> Self {
        PluginError::IoError(v)
    }
}

impl<T: Error> From<capnp::Error> for PluginError<T> {
    fn from(v: capnp::Error) -> Self {
        PluginError::CapnpError(v)
    }
}

impl<T: Error> fmt::Display for PluginError<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            PluginError::IoError(e) => write!(f, "io error: {}", e),
            PluginError::CapnpError(e) => write!(f, "capnp error: {}", e),
            PluginError::HandlerError(e) => write!(f, "plugin error: {}", e),
        }
    }
}

// TODO is this static bound ok??
impl<T: Error + 'static> Error for PluginError<T> {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match self {
            PluginError::IoError(e) => Some(e),
            PluginError::CapnpError(e) => Some(e),
            PluginError::HandlerError(e) => Some(e),
        }
    }
}

pub struct Plugin<T: PluginHandler> {
    stdin: io::BufReader<io::Stdin>,
    stdout: io::BufWriter<io::Stdout>,

    handler: T,
}

impl<T: PluginHandler> Plugin<T> {
    pub fn new(handler: T) -> Self {
        let stdin = io::stdin();
        let stdout = io::stdout();

        Plugin {
            stdin: io::BufReader::new(stdin),
            stdout: io::BufWriter::new(stdout),

            handler,
        }
    }

    pub fn start(&mut self) -> Result<(), PluginError<T::Error>> {
        let mut message = capnp::message::Builder::new_default();

        {
            message.init_root::<protocol::ready::Builder>();
        };

        capnp_ser::write_message(&mut self.stdout, &message)?;
        self.stdout.flush()?;

        loop {
            let msg = capnp_ser::read_message(&mut self.stdin, ReaderOptions::new())?;
            let request = msg.get_root::<protocol::request::Reader>()?;

            match request.which() {
                Ok(protocol::request::Which::Types(_)) => {
                    // map_err required, maybe fixed by inequality bound?
                    // (if that ever gets implemented)
                    self.handler.typedefs().map_err(PluginError::HandlerError)?;
                }
                Ok(protocol::request::Which::Shutdown(())) => break,
                _ => unimplemented!(),
            }
        };

        Ok(())
    }
}
