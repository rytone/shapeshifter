# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/3.0/.

@0xf4782923a4a344bd;

using Types = import "types.capnp";
using Plugin = import "plugin.capnp";

const ver :Text = "0.0.2";

struct Ready {
    version @0 :Text = .ver;
}

# incomplete
struct Typedef {
    name @0 :Text;
}

struct GenCommand {
    name @0 :Text;
    args @1 :List(Arg);
}

struct Value {
    type @0 :Text;
    name @1 :Text;
}

struct GenRequirements {
    needs @0 :List(Value);
    provides @1 :List(Value);
}

struct MangledName {
    value @0: Value;
    mangled @1: Text;
}

struct GenRegion {
    regionName @0 :Text;
    code @1 :Text;
}

# TODO someday
# interface Plugin {
#     types @0 () -> (list :List(Typedef));
#     command @1 (command :GenCommand);
#     requirements @2 () -> (reqs :GenRequirements);
#     generate @3 (mangled :List(MangledName)) -> (regions :List(GenRegion));
#     reset @4 ();
# }

# plugin request
struct Request {
    union {
        types @0 :Void;
        genCommand @1 :GenCommand;
        requirements @2 :Void;
        generate @3 :List(MangledName);
        reset @4 :Void;
        shutdown @5 :Void;
    }
}

# plugin responses (some are already defined, reorg TODO)
struct Typedefs {
    types @0 :List(Typedef);
}

struct GenRegions {
    regions @0 :List(GenRegion);
}

struct Complete {
    # no results...
}
