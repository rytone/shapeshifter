/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#[test]
fn crate_version_matches_spec() {
    assert_eq!(env!("CARGO_PKG_VERSION"), crate::VER);
}
