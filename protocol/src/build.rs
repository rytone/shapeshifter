/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

fn main() -> capnp::Result<()> {
    capnpc::CompilerCommand::new()
        .file("shapeshifter.capnp")
        // FIXME capnp pointer defaults were not tested on 2018 :/
        // .edition(capnpc::RustEdition::Rust2018)
        .run()
}
