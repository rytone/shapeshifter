# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/3.0/.

@0xda78281a7353dd2c

# TODO somehow merge this with `Literal`?
enum LiteralType {
    bool @0;
    string @1;
    int @2;
    uint @3;
    float @4;
}

struct TypeDef {
    name @0 :String;
    expressableBy @1 :List(LiteralType);
}

struct ParamType {
    union {
        defined @0 :TypeDef;
        literal @1 :LiteralType;
    }
}

struct CommandDef {
    name @0 :Text;
    params @1 :List(ParamType);
}

struct Literal {
    union {
        bool @0 :Bool;

        string @1 :Text;

        int @2 :Int64;
        uint @3 :UInt64;
        float @4 :Float64;
    }
}

struct Arg {
    union {
        literal @0 :Literal;
        variable @1 :Text;
    }
}

struct GenCommand {
    name @0 :Text;
    args @1 :List(Arg);
}

struct Value {
    type @0 :Text;
    name @1 :Text;
}

struct MangledName {
    value @0 :Value;
    mangled @1 :Text;
}
