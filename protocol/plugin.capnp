# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/3.0/.

@0xd24c8d8849458c14

using Types = import "types.capnp";

# TODO switch to proper rpc someday

struct Request {
    union {
        capabilities @0 :Void;
        loadCommand @2 :Types.GenCommand;
        provides @3 :Void;
        generate @4 :Types.MangledName;
    }
}
