/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use clap::{AppSettings, clap_app, crate_version, self};

pub fn get() -> clap::App<'static, 'static> {
    clap_app!(Shapeshifter =>
        (global_setting: AppSettings::DisableHelpSubcommand)
        (global_setting: AppSettings::VersionlessSubcommands)
        (version: crate_version!())

        (@setting SubcommandRequiredElseHelp)

        (@subcommand global =>
            (@setting SubcommandRequiredElseHelp)

            (about: "Manage plugins and other global configuration")
            (alias: "g")

            (@subcommand list =>
                (about: "List installed interfaces and plugins")
                (alias: "l")
            )

            (@subcommand install =>
                (about: "Install new interfaces and/or plugins")
                (alias: "i")

                (@arg URL: +required "URL to install from, e.g. gitlab.com/rytone/shapeshifter:base@0.1.0")
                (@arg SHOW_OUTPUT: --("show-output") -o "Display output while compiling plugins")
                (@arg NO_CONFIRM: --("no-confirm") -c "Do not prompt to confirm installation. Use at your own risk, untrusted build scripts may be malicious.")
                (@arg FORCE: --force -f "Overwrite existing installed files")
            )

            (@subcommand uninstall =>
                (about: "Uninstall interfaces and/or plugins")
                (alias: "u")

                (@arg URL: +required "URL to uninstall everything from, e.g. gitlab.com/rytone/shapeshifter:base@0.1.0")
                (@arg NO_CONFIRM: --("no-confirm") -c "Do not prompt to confirm uninstallation")
                (@arg FORCE: --force -f "Ignore broken dependencies. Use at your own risk.")
            )
        )
        
        (@subcommand init => 
            (about: "Initialize a new project in the current directory")
            (alias: "i")
        )

        (@subcommand build =>
            (about: "Build the current project")
            (alias: "b")
        )

        (@subcommand clean => 
            (about: "Remove build artifacts of this project")
            (alias: "c")
        )

        (@subcommand test =>
            (@setting Hidden)

            (about: "special zone")
            (alias: "t")

            (@arg EXECUTABLE: +required "run this one")
        )
    )
}
