/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::io::{self, BufRead, BufReader, BufWriter, Write};
use std::process::{Command, Stdio};
use std::thread;

use capnp::message::ReaderOptions;
use capnp::serialize as capnp_ser;
use shapeshifter_protocol as protocol;
use snafu::{OptionExt, ResultExt, Snafu};

#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("failed to start plugin: {}", source))]
    PluginStart { source: io::Error },
    #[snafu(display("failed to open {}", ty))]
    NoStdio { ty: &'static str },
    #[snafu(display("incompatible plugin version: {} vs {}", cli, plugin))]
    VersionMismatch { cli: &'static str, plugin: String },
    #[snafu(display("failed to read a new message from the plugin: {}", source))]
    ReadError { source: capnp::Error },
    #[snafu(display("failed to deserialize a message from the plugin: {}", source))]
    DeserializeError { source: capnp::Error },
    #[snafu(display("plugin sent an invalid version ({}): {}", version, source))]
    InvalidVersion { version: String, source: semver::SemVerError },
    #[snafu(display("failed to write a message to the plugin: {}", source))]
    WriteError { source: io::Error }
}

pub fn main<'a>(exec: &'a str) -> Result<(), Error> {
    println!("starting {}", exec);

    let command = Command::new(exec)
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()
        .context(PluginStart)?;

    let mut cmd_stdin = BufWriter::new(
        command.stdin.context(NoStdio { ty: "stdin" })?
    );
    let mut cmd_stdout = BufReader::new(
        command.stdout.context(NoStdio { ty: "stdout" })?
    );
    let cmd_stderr = BufReader::new(
        command.stderr.context(NoStdio { ty: "stderr" })?
    );

    let exec_owned = exec.to_string();
    let stderr_thread = thread::spawn(move || {
        let mut lines = cmd_stderr.lines();
        loop {
            match lines.next() {
                Some(Ok(line)) => println!("[{}] {}", exec_owned, line),
                Some(Err(e)) => {
                    // TODO something better
                    println!("stderr err: {}", e);
                    break;
                },
                None => break,
            }
        };
    });

    println!("listening for ready message...");

    // TODO capnp example uses unbuffered io and locks the handle mutex
    // but the docs suggest using buffered io.
    // what should actually be used?
    let msg = capnp_ser::read_message(&mut cmd_stdout, ReaderOptions::new())
        .context(ReadError)?;
    let ready_msg = msg.get_root::<protocol::ready::Reader>()
        .context(DeserializeError)?;
    let plugin_ver = ready_msg.get_version().context(DeserializeError)?;

    println!("got version {}", plugin_ver);

    // TODO maybe use tilde requirement instead of caret
    // how do you even make semver-compatible changes to the protocol?
    let ver_req = semver::VersionReq::parse(protocol::VER)
        .expect("generator was built with an invalid protocol version");
    let plugin_ver = semver::Version::parse(plugin_ver)
        .with_context(|| InvalidVersion { version: plugin_ver.to_string() })?;

    if !ver_req.matches(&plugin_ver) {
        return Err(Error::VersionMismatch {
            cli: protocol::VER,
            plugin: plugin_ver.to_string()
        })
    }

    println!("protocol versions compatible");

    let mut typedef_req = capnp::message::Builder::new_default();
    {
        let mut req = typedef_req.init_root::<protocol::request::Builder>();
        req.set_types(());
    };

    capnp_ser::write_message(&mut cmd_stdin, &typedef_req).context(WriteError)?;
    cmd_stdin.flush().context(WriteError)?;

    println!("shutting down executable");

    let mut shutdown_req = capnp::message::Builder::new_default();
    {
        let mut req = shutdown_req.init_root::<protocol::request::Builder>();
        req.set_shutdown(());
    }

    capnp_ser::write_message(&mut cmd_stdin, &shutdown_req).context(WriteError)?;
    cmd_stdin.flush().context(WriteError)?;

    // wait for stderr
    // TODO remove expect?
    stderr_thread.join().expect("stderr thread panicked");

    Ok(())
}
