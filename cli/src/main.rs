/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

mod args;
mod subcommands;

fn main() {
    let matches = args::get().get_matches();

    if let Some(test_matches) = matches.subcommand_matches("test") {
        let exec = test_matches.value_of("EXECUTABLE").unwrap();

        if let Some(err) = subcommands::test::main(exec).err() {
            // TODO prettier error reporting
            println!("error: {}", err);
        }
        return;
    }

    unimplemented!();
}
