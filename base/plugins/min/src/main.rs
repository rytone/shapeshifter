/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::io::{self, Write};

use shapeshifter_framework::{Plugin, PluginHandler};

struct Handler;
impl PluginHandler for Handler {
    type Error = io::Error;

    fn typedefs(&mut self) -> Result<(), io::Error> {
        writeln!(io::stderr(), "broh,,,")
    }
}

fn main() {
    let mut plugin = Plugin::new(Handler);

    plugin.start().expect("could not run plugin");
}
