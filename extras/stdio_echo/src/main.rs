/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::io::{self, Write};

fn main() -> io::Result<()> {
    let stdin = io::stdin();
    let mut stdout = io::stdout();

    let mut buf = String::new();
    loop {
        buf.clear();
        stdin.read_line(&mut buf)?;

        buf = buf.trim().to_string();

        write!(stdout, "{}!\n", buf)?;
        stdout.flush()?;
    }
}
